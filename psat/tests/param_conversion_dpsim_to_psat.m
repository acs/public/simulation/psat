% d_kundur2_example1_dpsim

% Line1
u_3ph=400;
s_3ph=2220;
z_base = u_3ph*u_3ph/s_3ph

r_line_pu = 0.001
r_line = r_line_pu*z_base

x_line1_pu = 0.5;
x_line1 = x_line1_pu*z_base

% Transf1
u_3ph=400;
s_3ph=2220;
z_base = u_3ph*u_3ph/s_3ph

x_transf1_pu = 0.15;
x_transf1 = x_transf1_pu*z_base

% Fault
u_3ph=400;
s_3ph=2220;
z_base = u_3ph*u_3ph/s_3ph

r_fault_pu = 0.001;
r_fault = r_fault_pu*z_base

% Syngen
u_3ph=400;
s_3ph=2220;
z_base = u_3ph*u_3ph/s_3ph

sg_xpd_pu = 0.3;
sg_xpd = sg_xpd_pu*z_base